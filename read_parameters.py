#!/opt/anaconda3/bin/python
# -*- coding: utf-8 -*-"""
===============
read parameters
===============
From 'Dynamic regimes in planetary cores: tau-ell diagrams', Nataf H-C. and N. Schaeffer, Compte-rendu Geosciences (2024).Parameters for tau-ell regime diagrams for various planets, simulations and lab experiments
Parameters from https://nssdc.gsfc.nasa.gov/planetary/factsheet/ unless otherwise specified
Earth : from Olson's introductory chapter of Treatise on Geophysics, unless otherwise specified
Astronomical symbols from: https://en.wikipedia.org/wiki/Astronomical_symbols#Symbols_for_the_planets
Unicode characters : https://www.w3.org/TR/xml-entity-names/026.html

231116 HCN : introduce red dwarfs231107 HCN : observed energy spectra slopes given for n-spectrum rather than for tauell
230406 HCN : a few slight modifications
230314 HCN : rearrangement
230308 HCN : toilettage
220925 HCN : *** en cours ***
"""
import numpy as np
# -------------------------
def read_parameters(planet):
# -------------------------
    param = {} # initiate dictionary of parameters
    mu0 = 4e-7*np.pi # SI magnetic permeability
    stefan = 5.67e-8 # [W m^-2 K^-4] Stefan's constant
    year = 365*24*3600 # [s] one year# ----------------------
    if planet == 'Earth':
# ----------------------
        print('Getting Earth core parameters...')
# properties of the Earth's core
# from the introduction chapter of the Treatise by Peter Olson, unless otherwise specified
        eta = 1. # [m2 s-1] (±0.5) magnetic diffusivity
        nu = 1.e-6 # [m2 s-1] kinematic viscosity (exposant ±2)
        kappa = 5.e-6 # [m2 s-1] (±3) thermal diffusivity --> nouvelle valeur pour Pr=0.2
        kappa_chi = 1.e-9 # [m2 s-1] compositional diffusivity (exposant ±2)
        alpha = 1.2e-5 # [K-1] (±0.5) coefficient of thermal expansion
        C_p = 850. # [J kg-1 K-1] (±20) 
        gravity = 8 # [m s-2] gravity
        V_P = 9000. # [m s-1] P-wave velocity
        Omega = 2.*np.pi/(24.*3600.) # [s-1] angular velocity
        M_o = 1.835e24 # [kg] mass of the outer core
        R_o = 3.480e6 # [m] radius of the core
        R_i = 1.22e6 # [m] radius of the inner core
        V_o = 4./3.*np.pi*(R_o**3-R_i**3) # [m^3] volume of the outer core
        rho = M_o/V_o # [kg m^-3] average density
        P_diss = 3.e12 # [W] dissipated power (from Landeau+ 2022)
        B_0 = 3.e-3 # [T] typical magnetic intensity in the core (2-4)
#
        t_SV = 6.e9*R_o/(R_o-R_i) # [s] overturn time of the largest vortex (l = R_o) from SV core flow inversion
        SV_slope = 0 # slope of the LW kinetic energy spectrum at large scale (flat)
#        l_SV_min = R_o/12 # minimum ell for this spectral trend (N. Gillet, personal com)
        l_SV_min = R_o/10 # minimum ell for this spectral trend (N. Gillet, personal com)
#
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer core radius
        A0_slope = 0 # tau-ell slope of the LW magnetic energy spectrum at large scale (flat)
#        l_A0_min = R_o/12 # minimum ell for this spectral trend
        l_A0_min = R_o/10 # minimum ell for this spectral trend
#        planet_symbol = "\u2641" # NB: "\u1F728" not working# --------------------------
    elif planet == 'Mercury':
# --------------------------
        print('Getting Mercury core parameters...')
# properties of the Earth's core (from the introduction chapter of the Treatise by Peter Olson)
        eta = 1. # [m2 s-1] (±0.5) magnetic diffusivity
        nu = 1.e-6 # [m2 s-1] kinematic viscosity (exposant ±2)
        kappa = 5.e-6 # [m2 s-1] (±3) thermal diffusivity --> nouvelle valeur pour Pr=0.2
        kappa_chi = 1.e-9 # [m2 s-1] compositional diffusivity (exposant ±2)
        alpha = 1.2e-5 # [K-1] (±0.5) coefficient of thermal expansion
        C_p = 850. # [J kg-1 K-1] (±20) 
        rho = 8e3 # [kg m^-3] density
        gravity = 3.7 # [m s-2] gravity
        V_P = 6000. # [m s-1] P-wave velocity
        Omega = 2.*np.pi/(1407.6*3600) # [s-1] angular velocity
        R_p = 2.440e6 # [m] radius of the planet
        R_o = 0.75*R_p # [m] radius of the core
        R_i = 0.5*R_p # [m] rayon de la graine
        M_o = 4*rho*np.pi/3 * (R_o**3 - R_i**3) # [kg] mass of the outer core
        P_diss = 1.e11 # [W] dissipated power (uncertain)
        B_0 = 6.e-6 # [T] typical magnetic intensity in the core (2-4)
        t_SV = 6.e8*R_o/(R_o-R_i) # [s] overturn time of the largest vortex (l = R_o) from SV core flow inversion
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer core radius
        planet_symbol = "\u263F"#
# ------------------------
    elif planet == 'Venus':
# ------------------------
        print('Getting Venus core parameters...')
# parameters copied from Earth except rotation rate and magnetic field intensity
        eta = 1. # [m2 s-1] (±0.5) magnetic diffusivity
        nu = 1.e-6 # [m2 s-1] kinematic viscosity (exposant ±2)
        kappa = 5.e-6 # [m2 s-1] (±3) thermal diffusivity --> nouvelle valeur pour Pr=0.2
        kappa_chi = 1.e-9 # [m2 s-1] compositional diffusivity (exposant ±2)
        alpha = 1.2e-5 # [K-1] (±0.5) coefficient of thermal expansion
        C_p = 850. # [J kg-1 K-1]
        rho = 10e3 # [kg m^-3] density
        gravity = 8.87 # [m s-2] gravity
        V_P = 9000. # [m s-1] P-wave velocity
        Omega = 2.*np.pi/(243*24.*3600.) # [s-1] angular velocity
        M_o = 1.835e24 # [kg] mass of the outer core
        R_o = 3.480e6 # [m] radius of the core
        R_i = 0 # [m] inner core radius
        P_diss = 3.e12 # [W] dissipated power ???
        B_0 = 1.e-7 # [T] typical magnetic intensity in the core (upper bound)
#        t_SV = 6.e9*R_o/(R_o-R_i) # [s] overturn time of the largest vortex (l = R_o) ??
        t_SV = np.NaN # [s] overturn time of the largest vortex (l = R_o) ??
##        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer core radius
        t_A0 = 1.e12 # [s] rounded for plot *** Alfvén time for B_0 and outer core radius
        planet_symbol = "\u2640"# ------------------------
    elif planet == 'Mars':
# ------------------------
        print('Getting Mars core parameters...')
# parameters copied from Earth except rotation rate and magnetic field intensity
        eta = 1. # [m2 s-1] (±0.5) magnetic diffusivity
        nu = 1.e-6 # [m2 s-1] kinematic viscosity (exposant ±2)
        kappa = 5.e-6 # [m2 s-1] (±3) thermal diffusivity --> nouvelle valeur pour Pr=0.2
        kappa_chi = 1.e-9 # [m2 s-1] compositional diffusivity (exposant ±2)
        alpha = 1.2e-5 # [K-1] (±0.5) coefficient of thermal expansion
        C_p = 850. # [J kg-1 K-1] (±20) 
        rho = 6e3 # [kg m^-3] density (5.7 to 6.3 from Stahler et al, 2021)
        gravity = 3.7 # [m s-2] gravity
        V_P = 5500. # [m s-1] P-wave velocity (Irving+ 2023)
        Omega = 2.*np.pi/(((24*60)+37)*60) # [s-1] angular velocity
        R_o = 1800e3 # [m] radius of the core (1830±40 from Stahler+ 2021; 1800 Irving+ 2023)
        R_i = 0 # [m] inner core radius
        M_o = 4*rho*np.pi/3*R_o**3 # [kg] mass of the outer core
        P_diss = 1.e11 # [W] dissipated power ??
        B_0 = 1.e-7 # [T] typical magnetic intensity in the core (upper bound)
        t_SV = 6.e8*R_o/(R_o-R_i) # [s] overturn time of the largest vortex (l = R_o) ??
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer core radius
        planet_symbol = "\u2642"# --------------------------
    elif planet == 'Ganymede':
# --------------------------
        print('Getting Ganymede core parameters...')
# properties of the Earth's core (from the introduction chapter of the Treatise by Peter Olson)
        eta = 1. # [m2 s-1] (±0.5) magnetic diffusivity
        nu = 1.e-6 # [m2 s-1] kinematic viscosity (exposant ±2)
        kappa = 5.e-6 # [m2 s-1] (±3) thermal diffusivity --> nouvelle valeur pour Pr=0.2
        kappa_chi = 1.e-9 # [m2 s-1] compositional diffusivity (exposant ±2)
        alpha = 1.2e-5 # [K-1] (±0.5) coefficient of thermal expansion
        C_p = 850. # [J kg-1 K-1] (±20) 
        rho = 6e3 # [kg m^-3] density ??
        gravity = 1.43 # [m s-2] gravity
        V_P = 5000. # [m s-1] P-wave velocity ??
        Omega = 2.*np.pi/(171.7*3600) # [s-1] angular velocity
        R_p = 5262e3/2 # [m] radius of the planet
        R_o = 500e3 # [m] radius of the core
        R_i = 0. # [m] rayon de la graine
        M_o = 4*rho*np.pi/3 * (R_o**3 - R_i**3) # [kg] mass of the outer core
# delta_rho = 1.e-9*rho # tiré de la co-densité typique de Loper TOG
# delta_rho = 1.e-16*rho # pour donner t_SV comme temps convectif si u proportionnelle à Ra^1/2
# delta_rho/rho calculé selon régime
        P_diss = 1.e9 # [W] dissipated power ??
        B_0 = 6.e-6 # [T] typical magnetic intensity in the core
        t_SV =3206.e8*R_o/(R_o-R_i) # [s] overturn time of the largest vortex (l = R_o) ??
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer core radius
#
# ----------------------
    elif planet == 'Jupiter':
# ----------------------
        print('Getting Jupiter core parameters...')
# properties of Jupiter's core (from the French et al (2012) and Sharan et al (2022)
        eta = 1.5 # [m2 s-1] magnetic diffusivity
        nu = 0.3e-6 # [m2 s-1] kinematic viscosity
        kappa = 1e-5 # [m2 s-1] thermal diffusivity
        kappa_chi = 0.2e-6 # [m2 s-1] compositional diffusivity
        alpha = 2e-5 # [K-1] coefficient of thermal expansion
        C_p = 1.e4 # [J kg-1 K-1]
        rho = 1200 # [kg m^-3] density
        gravity = 23 # [m s-2] gravity
        V_P = 20000. # [m s-1] P-wave velocity
        Omega = 2.*np.pi/(9.9*3600.) # [s-1] angular velocity
        R_J = 142984e3/2 # [m] radius of the planet
        R_o = 0.83*R_J # [m] radius of the core (from Sharan et al, 2022)
        R_i = 0.68*R_J # [m] radius of the "inner" core ??
        M_o = 4*rho/3*(R_o**3 - R_i**3) # [kg] mass of the outer core
        P_diss = 5*4*np.pi*R_J**2/10 # [W] dissipated power (5 W/m^2 total heat flux --> divided by 10)
        B_0 = 25.e-3 # [T] typical magnetic intensity in the core (infered from Sharan et al, 2022)
#        Rm = 1000 # from Sharan et al (2022)
#        t_SV = 2*np.pi*(R_o-R_i)*R_o/eta/Rm # [s] overturn time of the largest vortex (l = R_o) from Rm
        t_SV = 2*np.pi*R_o/0.01 # [s] overturn time of the largest vortex (l = R_o) from u=0.04 m/s (Connerney et al, 2022) --> 0.01 m/s after Bloxham correction (Chjris Jones)
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer core radius
        planet_symbol = "\u2643"#
# ----------------------
    elif planet == 'S2':
# ----------------------
        print('Getting S2 parameters...')
# properties of S2 DNS simulation of Schaeffer et al (2017)
# scales : length=dd, time=dd^2/nu. rho_simu=1
        Ekman_simu = 1e-7 # nu/Omega/d^2
#        Lehnert_simu = 1.3e-3
#        Rossby_simu = 6e-4 # U_rms/Omega/d
        Pm_simu = 0.1 # nu/eta
        Pr_simu = 1 # nu/kappa
        Ra_truc_simu = 2.4e13 # Ra/beta/R_o needed to determine gravity
        P_conv_simu = 10.3e13 # adim P_diss
        P_conv_eta = 8.71e13 # adim ohmic P_diss
        P_conv_nu = 1.44e13 # adim ohmic P_diss
#
        Omega = 2.*np.pi/(24.*3600.) # [s-1] angular velocity
        R_o = 3.480e6 # [m] radius of the core
        R_i = 0.35*R_o # [m] radius of the inner core
        dd = R_o-R_i # [m] outer core thickness
        M_o = 1.835e24 # [kg] mass of the outer core
        rho = M_o/(4/3*np.pi*(R_o**3-R_i**3)) # [kg m^-3] density (actual average density of the Earth's outer core)
#
# from Nathanael's scalings
        nu = Omega*dd**2*Ekman_simu # [m2 s-1] kinematic viscosity
        eta = nu/Pm_simu # [m2 s-1] magnetic diffusivity
        kappa = nu/Pr_simu # [m2 s-1] thermal diffusivity
        P_diss = P_conv_simu*rho*nu**3/dd # [W] dissipated power
        P_diss_eta = P_conv_eta*rho*nu**3/dd # [W] magnetic dissipated power
        P_diss_nu = P_conv_nu*rho*nu**3/dd # [W] viscous dissipated power
        gravity = Ra_truc_simu*R_o*nu**2/Pr_simu/dd**4 # [m s-2] gravity (deduced from Ra_truc_simu)
#
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = float("NaN") # [K-1] (±0.5) coefficient of thermal expansion
        C_p = float("NaN") # [J kg-1 K-1] (±20) 
        V_P = float("NaN") # [m s-1] P-wave velocity
        t_SV = float("NaN") # to be determined from spectrum
        t_A0 = float("NaN") # to be determined from spectrum
        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0
        B_0 = float("NaN") # (T] typical magneticfield intensity
#
# filenames of n-spectra
        file_spec_u = 'DNS_Schaeffer/spec_S2_u.txt'
        file_spec_b = 'DNS_Schaeffer/spec_S2_b.txt'
        file_spec_rho = 'DNS_Schaeffer/spec_S2_rho.txt'

#
# ----------------------------
    elif planet == 'S2_check':
# ----------------------------
        print('Getting S2 parameters...')
# properties of S2 DNS simulation of Schaeffer et al (2017) in original units
# scales : dd=1, dd^2/nu=1. rho_simu=1, mu_simu=1
        Ekman_simu = 1e-7 # nu/Omega/d^2
#        Lehnert_simu = 1.3e-3
#        Rossby_simu = 6e-4 # U_rms/Omega/d
        Pm_simu = 0.1 # nu/eta
        Pr_simu = 1 # nu/kappa
        Ra_truc_simu = 2.4e13 # Ra/beta/R_o needed to determine gravity
        P_conv_simu = 10.3e13 # adim P_diss
        P_conv_eta = 8.71e13 # adim ohmic P_diss
        P_conv_nu = 1.44e13 # adim ohmic P_diss
#
        dd = 1 
        R_o = 1/(1-0.35) # radius of the core
        R_i = 0.35*R_o # radius of the inner core
        rho = 1 # density
        M_o = rho*4/3*np.pi*(R_o**3-R_i**3) # mass of the outer core
#
# from Nathanael's scalings
        nu = 1 # kinematic viscosity
        eta = nu/Pm_simu #  magnetic diffusivity
        kappa = nu/Pr_simu # thermal diffusivity
        Omega = nu**2/dd**2/Ekman_simu # angular velocity
        P_diss = P_conv_simu # dissipated power
        P_diss_eta = P_conv_eta # magnetic dissipated power
        P_diss_nu = P_conv_nu # viscous dissipated power
        gravity = Ra_truc_simu*R_o*nu**2/Pr_simu/dd**4 # gravity (deduced from Ra_truc_simu)
#
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = float("NaN") # [K-1] (±0.5) coefficient of thermal expansion
        C_p = float("NaN") # [J kg-1 K-1] (±20) 
        V_P = float("NaN") # [m s-1] P-wave velocity
        t_SV = float("NaN") # to be determined from spectrum
        t_A0 = float("NaN") # to be determined from spectrum
        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0
        B_0 = float("NaN") # (T] typical magneticfield intensity
#
# filenames of n-spectra
        file_spec_u = 'DNS_Schaeffer/spec_S2_u.txt'
        file_spec_b = 'DNS_Schaeffer/spec_S2_b.txt'
        file_spec_rho = 'DNS_Schaeffer/spec_S2_rho.txt'
#
# --------------------------------
    elif planet == 'Dormy_strong':
# --------------------------------
        print('Getting Dormy (2016) strong parameters...')
# properties of high-Pm strong-field DNS simulation of Dormy (2016)
        Ekman_simu = 3e-4 # nu/Omega/d^2
        Pm_simu = 18 # nu/eta
        Pr_simu = 1 # nu/kappa
        Ra_truc_simu = 3.648e5 # Ra/beta/R_o needed to determine gravity
        P_conv_simu = 650e3 # adim P_diss
        P_conv_nu = 250e3 # adim P_diss
        P_conv_eta = 400e3 # adim P_diss
#
        Omega = 2.*np.pi/(24.*3600.) # [s-1] angular velocity
        R_o = 3.480e6 # [m] radius of the core
        R_i = 0.35*R_o # [m] radius of the inner core
        dd = R_o-R_i # [m] outer core thickness
#
        M_o = 1.835e24 # [kg] mass of the outer core
        rho = M_o/(4/3*np.pi*(R_o**3-R_i**3)) # [kg m^-3] density (actual average density 
# from Nathanael's scalings
        nu = Omega*dd**2*Ekman_simu # [m2 s-1] kinematic viscosity
        eta = nu/Pm_simu # [m2 s-1] magnetic diffusivity
        kappa = nu/Pr_simu # [m2 s-1] thermal diffusivity
        P_diss = P_conv_simu*rho*nu**3/dd # [W] dissipated power
        P_diss_eta = P_conv_eta*rho*nu**3/dd # [W] magnetic dissipated power
        P_diss_nu = P_conv_nu*rho*nu**3/dd # [W] viscous dissipated power
        gravity = Ra_truc_simu*R_o*nu**2/Pr_simu/dd**4 # [m s-2] gravity (deduced from Ra_truc_simu)
#
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = float("NaN")# [K-1] (±0.5) coefficient of thermal expansion
        C_p = float("NaN") # [J kg-1 K-1] (±20) 
        V_P = float("NaN") # [m s-1] P-wave velocity
        t_SV = float("NaN") # to be determined from spectrum
        t_A0 = float("NaN") # to be determined from spectrum
        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0
        B_0 = float("NaN")
#
# filenames of n-spectra
        file_spec_u = 'DNS_Dormy/spec_dormy16strong_u.txt'
        file_spec_b = 'DNS_Dormy/spec_dormy16strong_b.txt'
        file_spec_rho = 'DNS_Dormy/spec_dormy16strong_rho.txt'
#
# ------------------------------
    elif planet == 'Dormy_weak':
# ------------------------------
        print('Getting Dormy (2016) weak parameters...')
# properties of high-Pm strong-field DNS simulation of Dormy (2016)
        Ekman_simu = 3e-4 # nu/Omega/d^2
        Pm_simu = 18 # nu/eta
        Pr_simu = 1 # nu/kappa
        Ra_truc_simu = 3.344e5 # Ra/beta/R_o needed to determine gravity
        P_conv_simu = 224e3 # adim P_diss
        P_conv_nu = 214e3 # adim P_diss
        P_conv_eta = 10.4e3 # adim P_diss
#
        Omega = 2.*np.pi/(24.*3600.) # [s-1] angular velocity
        R_o = 3.480e6 # [m] radius of the core
        R_i = 0.35*R_o # [m] radius of the inner core
        dd = R_o-R_i # [m] outer core thickness
#
        M_o = 1.835e24 # [kg] mass of the outer core
        rho = M_o/(4/3*np.pi*(R_o**3-R_i**3)) # [kg m^-3] density (actual average density 
# from Nathanael's scalings
        nu = Omega*dd**2*Ekman_simu # [m2 s-1] kinematic viscosity
        eta = nu/Pm_simu # [m2 s-1] magnetic diffusivity
        kappa = nu/Pr_simu # [m2 s-1] thermal diffusivity
        P_diss = P_conv_simu*rho*nu**3/dd # [W] dissipated power
        P_diss_eta = P_conv_eta*rho*nu**3/dd # [W] magnetic dissipated power
        P_diss_nu = P_conv_nu*rho*nu**3/dd # [W] viscous dissipated power
        gravity = Ra_truc_simu*R_o*nu**2/Pr_simu/dd**4 # [m s-2] gravity (deduced from Ra_truc_simu)
#
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = float("NaN")# [K-1] (±0.5) coefficient of thermal expansion
        C_p = float("NaN") # [J kg-1 K-1] (±20) 
        V_P = float("NaN") # [m s-1] P-wave velocity
        t_SV = float("NaN") # to be determined from spectrum
        t_A0 = float("NaN") # to be determined from spectrum
        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0
        B_0 = float("NaN")
#
# filenames of n-spectra
        file_spec_u = 'DNS_Dormy/spec_dormy16weak_u.txt'
        file_spec_b = 'DNS_Dormy/spec_dormy16weak_b.txt'
        file_spec_rho = 'DNS_Dormy/spec_dormy16weak_rho.txt'
#
        spec_trim = True # flag to trim spectra from wiggles due to symmetry properties
#
# ------------------------------
    elif planet == 'Guervilly':
# ------------------------------
        print('Getting Guervilly et al (2019) parameters...')
# properties of Ek = 10^-8 rotating full sphere 3D simulation of Guervilly et al (2019)
        Ekman_simu = 1e-8 # nu/Omega/d^2
        Pr_simu = 0.01 # nu/kappa
        Ra_simu = 2.5e10 # Ra needed to determine gravity
        P_conv_simu = 1.47e13 # adim nu P_diss (mostly QG)
        P_conv_nu = 7e12 # adim nu P_diss (in the bulk)
#
        Omega = 2.*np.pi/(24.*3600.) # [s-1] angular velocity
        R_o = 3.480e6 # [m] radius of the core
        R_i = 0. # [m] radius of the inner core
        dd = R_o-R_i # [m] outer core thickness
#
        M_o = 1.835e24 # [kg] mass of the outer core
        rho = M_o/(4/3*np.pi*(R_o**3-R_i**3)) # [kg m^-3] density (actual average density 
# from Nathanael's scalings
        nu = Omega*dd**2*Ekman_simu # [m2 s-1] kinematic viscosity
        kappa = nu/Pr_simu # [m2 s-1] thermal diffusivity
        P_diss = P_conv_simu*rho*nu**3/dd # [W] dissipated power
        P_diss_nu = P_conv_nu*rho*nu**3/dd # [W] viscous dissipated power
        P_diss_nu_QG = P_diss - P_diss_nu # [W] viscous dissipated power in the Ekman layer
        gravity = Ra_simu*R_o*nu**2/Pr_simu/dd**4 # [m s-2] gravity (deduced from Ra_simu)
#
        eta = float("NaN") # [m2 s-1] magnetic diffusivity
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = float("NaN")# [K-1] (±0.5) coefficient of thermal expansion
        C_p = float("NaN") # [J kg-1 K-1] (±20) 
        V_P = float("NaN") # [m s-1] P-wave velocity
        t_SV = float("NaN") # to be determined from spectrum
        t_A0 = float("NaN") # to be determined from spectrum
        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0
        B_0 = float("NaN")
#
# filenames of n-spectra (or m-spectra)
#        file_spec_u = 'DNS_Guervilly/spec_guervilly19_E1e-8_u.txt'
        file_spec_u = 'DNS_Guervilly/spec-m_guervilly19_E1e-8_u.txt'
#        file_spec_u_r = 'DNS_Guervilly/spec_guervilly19_E1e-8_ur.txt'
        file_spec_u_r = 'DNS_Guervilly/spec-m_guervilly19_E1e-8_ur.txt'
#        file_spec_rho = 'DNS_Guervilly/spec_guervilly19_E1e-8_rho.txt'
        file_spec_rho = 'DNS_Guervilly/spec-m_guervilly19_E1e-8_rho.txt'
#
# --------------------------------    elif planet == 'Aubert_path_0':# --------------------------------        print('Getting Aubert path 0 parameters...')# properties of path 0 Sequence B simulation from Table 1 of Aubert (2023)# scales : length=2260 km, time=tau_eta=247000 year, density=11e3 kg/m3, gravity=10 m/s2        epsilon = 1 # path parameter
        tau_eta = 247000*year # [s] magnetic diffusion time
        gravity = 10 # [m s-2] # gravity        Ekman_eta_simu = 7.5e-6 # eta/Omega/d^2        Pm_simu = 4 # nu/eta        Pr_simu = 1 # nu/kappa        Ra_F_simu = 1.68e-5 # Ra_F needed to determine gravity *** a voir ***#        Omega = 1/Ekman_eta_simu/tau_eta # [s-1] angular velocity        R_o = 3.480e6 # [m] radius of the core        R_i = 0.35*R_o # [m] radius of the inner core        dd = R_o-R_i # [m] outer core thickness        M_o = 1.835e24 # [kg] mass of the outer core        rho = M_o/(4/3*np.pi*(R_o**3-R_i**3)) # [kg m^-3] density (actual average density of the Earth's outer core)## from Julien's scalings        eta = dd**2/tau_eta # [m2 s-1] magnetic diffusivity        nu = eta*Pm_simu # [m2 s-1] kinematic viscosity        kappa = nu/Pr_simu # [m2 s-1] thermal diffusivity#        gravity = Ra_F_simu*R_o*nu**2/Pr_simu/dd**4 # [m s-2] gravity (deduced from Ra_F_simu) *** a voir ***#        P_diss = 1.88e12/(1e7*epsilon)**(1/2) # [W] dissipated power *** for trial ***#        P_diss = 1.88e12 # [W] dissipated power *** for trial ***        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity        alpha = float("NaN") # [K-1] (±0.5) coefficient of thermal expansion        C_p = float("NaN") # [J kg-1 K-1] (±20)         V_P = float("NaN") # [m s-1] P-wave velocity        t_SV = 158*year # [s] from Table 2        t_A0 = 89*year # [s] from Table 2        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0        B_0 = float("NaN") # (T] typical magnetic field intensity## filenames of n-spectra (rho spectrum unavailable)
        file_spec_u = 'DNS_Aubert/spec_path_0_u.txt'
        file_spec_b = 'DNS_Aubert/spec_path_0_b.txt'
#
        object_type = 'planetary_core' # (for setting appropriate axes)## ---------------------------------    elif planet == 'Aubert_path_43':# ---------------------------------        print('Getting Aubert path 43 percent parameters...')# properties of path 43% Sequence B simulation from Table 1 of Aubert (2023)# scales : length=2260 km, time=tau_eta=247000 year, density=11e3 kg/m3, gravity=10 m/s2        epsilon = 1e-3 # path parameter        tau_eta = 247000*year # [s] magnetic diffusion time        gravity = 10 # [m s-2] # gravity        Ekman_eta_simu = 2.37e-7 # eta/Omega/d^2        Pm_simu = 0.126 # nu/eta        Pr_simu = 1 # nu/kappa        Ra_F_simu = 1.68e-8 # Ra_F needed to determine gravity *** a voir ***##        Omega = 2.*np.pi/(24.*3600.) # [s-1] angular velocity        Omega = 1/Ekman_eta_simu/tau_eta # [s-1] angular velocity        R_o = 3.480e6 # [m] radius of the core        R_i = 0.35*R_o # [m] radius of the inner core        dd = R_o-R_i # [m] outer core thickness        M_o = 1.835e24 # [kg] mass of the outer core        rho = M_o/(4/3*np.pi*(R_o**3-R_i**3)) # [kg m^-3] density (actual average density of the Earth's outer core)## from Julien's scalings#        eta = Omega*dd**2*Ekman_eta_simu # [m2 s-1] magnetic diffusivity        eta = dd**2/tau_eta # [m2 s-1] magnetic diffusivity        nu = eta*Pm_simu # [m2 s-1] kinematic viscosity        kappa = nu/Pr_simu # [m2 s-1] thermal diffusivity#        gravity = Ra_F_simu*R_o*nu**2/Pr_simu/dd**4 # [m s-2] gravity (deduced from Ra_F_simu) *** a voir ***#        P_diss = 3.31e12/(1e7*epsilon)**(1/2)  # [W] dissipated power *** for trial ***#        P_diss = 3.31e12*(1e7*epsilon)  # [W] dissipated power *** for trial ***#        P_diss = 3.31e12  # [W] dissipated power *** for trial ***        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity        alpha = float("NaN") # [K-1] (±0.5) coefficient of thermal expansion        C_p = float("NaN") # [J kg-1 K-1] (±20)         V_P = float("NaN") # [m s-1] P-wave velocity        t_SV = 124*year # [s] from Table 2        t_A0 = 15*year # [s] from Table 2        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0        B_0 = float("NaN") # (T] typical magnetic field intensity
## filenames of n-spectra (rho spectrum unavailable)
        file_spec_u = 'DNS_Aubert/spec_path_43_u.txt'
        file_spec_b = 'DNS_Aubert/spec_path_43_b.txt'

        object_type = 'planetary_core' # (for setting appropriate axes)# ----------------------------------    elif planet == 'Aubert_path_100':# ----------------------------------        print('Getting Aubert path 100 percent parameters...')# properties of path 100% Sequence B simulation from Table 1 of Aubert (2023) # scales : length=2260 km, time=tau_eta=247000 year, density=11e3 kg/m3, gravity=10 m/s2        epsilon = 1e-7 # path parameter        gravity = 10 # [m s-2] # gravity        tau_eta = 247000*year # [s] magnetic diffusion time        Ekman_eta_simu = 2.37e-9 # eta/Omega/d^2        Pm_simu = 1.26e-3 # nu/eta        Pr_simu = 1 # nu/kappa        Ra_F_simu = 1.68e-12 # Ra_F needed to determine gravity *** a voir ***##        Omega = 2.*np.pi/(24.*3600.) # [s-1] angular velocity        Omega = 1/Ekman_eta_simu/tau_eta # [s-1] angular velocity        R_o = 3.480e6 # [m] radius of the core        R_i = 0.35*R_o # [m] radius of the inner core        dd = R_o-R_i # [m] outer core thickness        M_o = 1.835e24 # [kg] mass of the outer core        rho = M_o/(4/3*np.pi*(R_o**3-R_i**3)) # [kg m^-3] density (actual average density of the Earth's outer core)## from Julien's scalings#        eta = Omega*dd**2*Ekman_eta_simu # [m2 s-1] magnetic diffusivity        eta = dd**2/tau_eta # [m2 s-1] magnetic diffusivity        nu = eta*Pm_simu # [m2 s-1] kinematic viscosity        kappa = nu/Pr_simu # [m2 s-1] thermal diffusivity#        gravity = Ra_F_simu*R_o*nu**2/Pr_simu/dd**4 # [m s-2] gravity (deduced from Ra_F_simu) *** a voir ***#        P_diss = 3.19e12/(1e7*epsilon)**(1/2) # [W] dissipated power #        P_diss = 3.19e12*(1e7*epsilon) # [W] dissipated power         kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity        alpha = float("NaN") # [K-1] (±0.5) coefficient of thermal expansion        C_p = float("NaN") # [J kg-1 K-1] (±20)         V_P = float("NaN") # [m s-1] P-wave velocity        t_SV = 129*year # [s] from Table 2        t_A0 = 1.5*year # [s] from Table 2        V_A = float("NaN") # [m s-1] vitesse des ondes d'Alfvén pour B_0        B_0 = float("NaN") # (T] typical magnetic field intensity## filenames of n-spectra (rho spectrum unavailable)
        file_spec_u = 'DNS_Aubert/spec_path_100_u.txt'
        file_spec_b = 'DNS_Aubert/spec_path_100_b.txt'

        object_type = 'planetary_core' # (for setting appropriate axes)# -----------------------
    elif planet == 'DTS':
# -----------------------
        print('Getting DTS experiment parameters...')
# properties of the DTS liquid sodium experiment (from Cabanes+ 2014)
        f_o = 10 # [Hz] outer sphere rotation frequency
        Df = 10 # [Hz] differential rotation of the inner sphere (150708S6X11)
        P_diss = 700 # [W] from Figure Power_fo_5_and_10_corrected.eps
#
        Omega = 2*np.pi*f_o # [s-1]  angular velocity of the container
#
        eta = 8.84e-2 # [m2 s-1] magnetic diffusivity
        nu = 6.5e-7 # [m2 s-1] kinematic viscosity
        kappa = float("NaN") # [m2 s-1] thermal diffusivity
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = float("NaN") # [K-1] coefficient of thermal expansion
        C_p = 1e3 # [J kg-1 K-1] 
        rho = 930 # [kg m^-3] density
        gravity = 9.81 # [m s-2] gravity
        V_P = 2700. # [m s-1] P-wave velocity
        R_o = 0.21 # [m] radius of the stainless steel shell
        R_i = 0.074 # [m] radius of the inner magnetized sphere
        M_o = 4./3*np.pi*(R_o**3 - R_i**3)*rho # [kg] mass of liquid sodium
        B_0 = 0.1 # [T] typical magnetic intensity from magnet
#        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer sphere radius
#
        Rm = 2*np.pi*Df*R_o**2/eta # magnetic Reynolds number
# Energy of the imposed dipole from Table III + normalization from Appendix A of Cabanes+ (2014)
        E_dipole = 23 * rho*R_o*eta**2
        t_A0 = np.sqrt((M_o*R_o**2)/E_dipole) # [s] Alfven time at l = R_o from the dipole energy
# kinetic energy from Table III + normalization from Appendix A of Cabanes+ (2014) 
        E_k = 0.35 * rho*R_o*eta**2*Rm**2
# induced magnetic energy from Table III + normalization from Appendix A of Cabanes+ (2014)       
        E_m = 4.4e-4 * rho*R_o*eta**2*Rm**2
        t_SV = np.sqrt((M_o*R_o**2)/E_k) # [s] overturn time at l = R_o from the kinetic energy
        t_b0 = np.sqrt((M_o*R_o**2)/E_m) # [s] Alfven time at l = R_o from the magnetic energy
# filenames of spectra
# kinetic energy times from the frequency spectrum of Doppler file48 of 060221
# f_o = 5 Hz, Df = 10 Hz, 40s-long set of aimuthal velocity profiles
# data extracted with UDV, tau-ell computed with plot_UDV_spectrum.m
# depth = 60:80mm (indices 93:125), moving average on 10 tau points
        file_spec_u = 'experiment_DTS/tau-ell_DTS_u.txt'
# magnetic time (from k-spectra)
# 150708S6X11 comp BB on meridian Q, 60s-long records
# f_o = 10.12, Deltaf = 9.95 
        file_spec_b = 'experiment_DTS/spec_DTS_b.txt'
#        
# -----------------------------
    elif planet == 'BigSister':
# -----------------------------
        print('Getting BigSister experiment parameters...')
# properties of the DTS liquid sodium experiment (from Nataf)
        f_o = 4 # [Hz] outer sphere rotation frequency
        Df = 13 # [Hz] differential rotation of the inner sphere
        P_diss = 10.e3 # [W] *** a voir *** dissipated power calculated from measurements
#
        Omega = 2*np.pi*f_o # [s-1]  angular velocity
        t_SV = (2*np.pi*Df)**(-1) # [s] overturn time of the largest vortex (l = R_o) from differential rotation
#
        eta = 8.84e-2 # [m2 s-1] magnetic diffusivity
        nu = 6.5e-7 # [m2 s-1] kinematic viscosity
        kappa = float("NaN") # [m2 s-1] thermal diffusivity 
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = float("NaN") # [K-1] coefficient of thermal expansion
        C_p = 1e3 # [J kg-1 K-1]
        rho = 930 # [kg m^-3] density
        gravity = 9.81 # [m s-2] gravity
        V_P = 2700. # [m s-1] P-wave velocity
        R_o = 1.46 # [m] radius of the stainless steel shell
        R_i = 0.51 # [m] radius of the inner magnetized sphere
        M_o = 4*rho/3*(R_o**3 - R_i**3) # [kg] mass of liquid sodium
        B_0 = 0.1 # [T] typical magnetic intensity ?
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer sphere radius
#
# ----------------------
    elif planet == 'GI846':
# ----------------------
        print('Getting red dwarf GI846 parameters...')
        R_sun = 695e6 # [m] volumetric mean radius of the Sun (NASA Sun facts)
        M_sun = 1.9885e30 # [kg] mass of the Sun (NASA Sun facts)
        day_convert = 2.*np.pi/(24*3600.) # [s-1] 2 pi/day
# properties of red dwarf GI846 from Donati et al (2023) (Tables 1, 2 and 3)
        T_eff = 3833 # [K] effective surface temperature
        log_g = 4.64 # log of gravity --> ???
        M_star = 0.57*M_sun # [kg] star mass
        R_o = 0.568*R_sun # [m] star radius
        B_0 = 0.1e-4 # [T] B_l = light-of-sight mean B from visible hemisphere
        Omega = day_convert/21.84 # [s-1] angular velocity
# other properties
        eta = 2e-2 # [m2 s-1] magnetic diffusivity (Zeldovich+ 1983) (QN) ???
        nu = 1e-4 # [m2 s-1] kinematic viscosity (Zeldovich+ 1983) (QN) ???
        kappa = 10 # [m2 s-1] thermal diffusivity (Zeldovich+ 1983) (QN) ???
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = 1/T_eff # [K-1] coefficient of thermal expansion (~1/T)
        C_p = 3.87e4 # [J kg-1 K-1] QN thesis ???
        gravity = 4 # [m s-2] gravity ???
        V_P = 170. # [m s-1] P-wave velocity (QN) ???
        rho = M_star/(4./3.*np.pi*R_o**3) # [kg m^-3] average density
        R_i = 0. # [m] inner radius of the convective zone
        V_o = 4./3.*np.pi*(R_o**3-R_i**3) # [m^3] volume of the convective zone
        M_o = rho*V_o # [kg] mass of the convective zone ***
#        P_diss = 1.e17 # [W] dissipated power ???
        P_diss = 4*np.pi*R_o**2*stefan*T_eff**4 # [W] dissipated power ???
#
        t_SV = float("NaN") # [s] overturn time from differential rotation ???
#
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer radius
        planet_symbol = "\u263C"
## ----------------------
    elif planet == 'GI382':
# ----------------------
        print('Getting red dwarf GI382 parameters...')
        R_sun = 695e6 # [m] volumetric mean radius of the Sun (NASA Sun facts)
        M_sun = 1.9885e30 # [kg] mass of the Sun (NASA Sun facts)
        day_convert = 2.*np.pi/(24*3600.) # [s-1] 2 pi/day
# properties of red dwarf GI846 from Donati et al (2023) (Tables 1, 2 and 3)
        T_eff = 3644 # [K] effective surface temperature
        log_g = 4.75 # log of gravity --> ???
        M_star = 0.51*M_sun # [kg] star mass
        R_o = 0.511*R_sun # [m] star radius
        B_0 = 1.3e-4 # [T] B_l = light-of-sight mean B from visible hemisphere
        Omega = day_convert/21.91 # [s-1] angular velocity
# other properties
        eta = 2e-2 # [m2 s-1] magnetic diffusivity (Zeldovich+ 1983) (QN) ???
        nu = 1e-4 # [m2 s-1] kinematic viscosity (Zeldovich+ 1983) (QN) ???
        kappa = 10 # [m2 s-1] thermal diffusivity (Zeldovich+ 1983) (QN) ???
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = 1/T_eff # [K-1] coefficient of thermal expansion (~1/T)
        C_p = 3.87e4 # [J kg-1 K-1] QN thesis ???
        gravity = 4 # [m s-2] gravity ???
        V_P = 170. # [m s-1] P-wave velocity (QN) ???
        rho = M_star/(4./3.*np.pi*R_o**3) # [kg m^-3] average density
        R_i = 0. # [m] inner radius of the convective zone
        V_o = 4./3.*np.pi*(R_o**3-R_i**3) # [m^3] volume of the convective zone
        M_o = rho*V_o # [kg] mass of the convective zone ***
#        P_diss = 1.e17 # [W] dissipated power ???
        P_diss = 4*np.pi*R_o**2*stefan*T_eff**4 # [W] dissipated power ???
#
        t_SV = float("NaN") # [s] overturn time from differential rotation ???
#
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer radius
        planet_symbol = "\u263C"
## ----------------------
    elif planet == 'GJ1246':
# ----------------------
        print('Getting red dwarf GJ1246 parameters...')
        R_sun = 695e6 # [m] volumetric mean radius of the Sun (NASA Sun facts)
        M_sun = 1.9885e30 # [kg] mass of the Sun (NASA Sun facts)
        day_convert = 2.*np.pi/(24*3600.) # [s-1] 2 pi/day
# properties of red dwarf GI846 from Donati et al (2023) (Tables 1, 2 and 3)
        T_eff = 2961 # [K] effective surface temperature
        log_g = 4.55 # log of gravity --> ???
        M_star = 0.12*M_sun # [kg] star mass
        R_o = 0.142*R_sun # [m] star radius
        B_0 = 15.4e-4 # [T] B_l = light-of-sight mean B from visible hemisphere
        Omega = day_convert/178 # [s-1] angular velocity
# other properties
        eta = 2e-2 # [m2 s-1] magnetic diffusivity (Zeldovich+ 1983) (QN) ???
        nu = 1e-4 # [m2 s-1] kinematic viscosity (Zeldovich+ 1983) (QN) ???
        kappa = 10 # [m2 s-1] thermal diffusivity (Zeldovich+ 1983) (QN) ???
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = 1/T_eff # [K-1] coefficient of thermal expansion (~1/T)
        C_p = 3.87e4 # [J kg-1 K-1] QN thesis ???
        gravity = 4 # [m s-2] gravity ???
        V_P = 170. # [m s-1] P-wave velocity (QN) ???
        rho = M_star/(4./3.*np.pi*R_o**3) # [kg m^-3] average density
        R_i = 0. # [m] inner radius of the convective zone
        V_o = 4./3.*np.pi*(R_o**3-R_i**3) # [m^3] volume of the convective zone
        M_o = rho*V_o # [kg] mass of the convective zone ***
#        P_diss = 1.e17 # [W] dissipated power ???
        P_diss = 4*np.pi*R_o**2*stefan*T_eff**4 # [W] dissipated power ???
#
        t_SV = float("NaN") # [s] overturn time from differential rotation ???
#
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer radius
        planet_symbol = "\u263C"#
# ----------------------
    elif planet == 'GI408':
# ----------------------
        print('Getting red dwarf GI408 parameters...')
        R_sun = 695e6 # [m] volumetric mean radius of the Sun (NASA Sun facts)
        M_sun = 1.9885e30 # [kg] mass of the Sun (NASA Sun facts)
        day_convert = 2.*np.pi/(24*3600.) # [s-1] 2 pi/day
# properties of red dwarf GI846 from Donati et al (2023) (Tables 1, 2 and 3)
        T_eff = 3487 # [K] effective surface temperature
        log_g = 4.79 # log of gravity --> ???
        M_star = 0.38*M_sun # [kg] star mass
        R_o = 0.39*R_sun # [m] star radius
        B_0 = 29e-4 # [T] B_l = light-of-sight mean B from visible hemisphere
        Omega = day_convert/171 # [s-1] angular velocity
# other properties
        eta = 2e-2 # [m2 s-1] magnetic diffusivity (Zeldovich+ 1983) (QN) ???
        nu = 1e-4 # [m2 s-1] kinematic viscosity (Zeldovich+ 1983) (QN) ???
        kappa = 10 # [m2 s-1] thermal diffusivity (Zeldovich+ 1983) (QN) ???
        kappa_chi = float("NaN") # [m2 s-1] compositional diffusivity
        alpha = 1/T_eff # [K-1] coefficient of thermal expansion (~1/T)
        C_p = 3.87e4 # [J kg-1 K-1] QN thesis ???
        gravity = 4 # [m s-2] gravity ???
        V_P = 170. # [m s-1] P-wave velocity (QN) ???
        rho = M_star/(4./3.*np.pi*R_o**3) # [kg m^-3] average density
        R_i = 0. # [m] inner radius of the convective zone
        V_o = 4./3.*np.pi*(R_o**3-R_i**3) # [m^3] volume of the convective zone
        M_o = rho*V_o # [kg] mass of the convective zone ***
#        P_diss = 1.e17 # [W] dissipated power ???
        P_diss = 4*np.pi*R_o**2*stefan*T_eff**4 # [W] dissipated power ???
#
        t_SV = float("NaN") # [s] overturn time from differential rotation ???
#
        t_A0 = R_o/(B_0/np.sqrt(rho*mu0)) # [s] Alfvén time for B_0 and outer radius
        planet_symbol = "\u263C"# ------------------------
    else:
        print('Undefined planet !...')
        return
        
# ------------------------
    dd = R_o-R_i # [m] outer core thickness
    V_A = B_0/np.sqrt(rho*mu0) # [m s-1] Alfvén wave velocity for B_0
#
    param.update({'eta':eta}) 
    param.update({'nu':nu}) 
    param.update({'kappa':kappa}) 
    param.update({'kappa_chi':kappa_chi})
    param.update({'alpha':alpha}) 
    param.update({'C_p':C_p}) 
    param.update({'rho':rho}) 
    param.update({'gravity':gravity}) 
    param.update({'V_P':V_P}) 
    param.update({'Omega':Omega}) 
    param.update({'M_o':M_o}) 
    param.update({'R_o':R_o}) 
    param.update({'R_i':R_i}) 
    param.update({'P_diss':P_diss}) 
    param.update({'B_0':B_0}) 
    param.update({'t_SV':t_SV})
    param.update({'t_A0':t_A0}) 
#
    param.update({'dd':dd}) 
    param.update({'V_A':V_A})
#
# additional optional properties in dictionary param
# --------------------------------------------------
# parameters added for planets
# - - - - - - - - - - - - - - - -
# observed spectral slope of large-scale velocity
    if 'SV_slope' in locals():
        param.update({'SV_slope':SV_slope}) # added for the Earth
        param.update({'l_SV_min':l_SV_min}) # added for the Earth
# observed spectral slope of large-scale magnetic field
    if 'A0_slope' in locals():
        param.update({'A0_slope':A0_slope}) # added for the Earth
        param.update({'l_A0_min':l_A0_min}) # added for the Earth
#
# parameters added for numerical simulations
# - - - - - - - - - - - - - - - -
    if 'P_diss_nu' in locals():
        param.update({'P_diss_nu':P_diss_nu}) # viscous dissipation
    if 'P_diss_nu_QG' in locals():
        param.update({'P_diss_nu_QG':P_diss_nu_QG}) # viscous dissipation
    if 'P_diss_eta' in locals():
        param.update({'P_diss_eta':P_diss_eta}) # ohmic dissipation
#
    if 'file_spec_u' in locals():
        param.update({'file_spec_u':file_spec_u}) # filename of u n-spectrum
    if 'file_spec_u_r' in locals():
        param.update({'file_spec_u_r':file_spec_u_r}) # filename of u_r n-spectrum
    if 'file_spec_b' in locals():
        param.update({'file_spec_b':file_spec_b}) # filename of b n-spectrum
    if 'file_spec_rho' in locals():
        param.update({'file_spec_rho':file_spec_rho}) # filename of rho n-spectrum
#
    if 'spec_trim' in locals():
        param.update({'spec_trim':spec_trim}) # flag to trim spectrum (symmetry)
#
# parameters added for laboratory experiments  
# - - - - - - - - - - - - - - - -
    if 't_b0' in locals():
        param.update({'t_b0':t_b0}) # observed induced magnetic field
#
# parameters added for decoration      
# - - - - - - - - - - - - - - - -
    if 'planet_symbol' in locals():
        param.update({'planet_symbol':planet_symbol}) # added for Venus
# -------------------------------------------------------
    return param # dictionary of all available parameters
