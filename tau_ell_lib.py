#!/opt/anaconda3/bin/python
#!/usr/bin/python3
#!/usr/bin/python3.9
"""
==========================
tau_ell_lib.py
Package of subroutines for tau-ell turbulent regime diagrams

230309 HCN : test new conversion from spectra to tau-ell
230309 HCN : new function to convert from spectra to tau-ell
220930 HCN
==========================
"""
import numpy as np
import matplotlib.pyplot as plt

# --------------------------------------------------------------------
def tauell_from_spectrum(var_type, spec_type, for_ell, for_tau, param):
    """
    # --------------------------------------------------------------------
    # converts energy spectra for_tau(for_ell) of various types spec_type
    # into (ell, tau_x) arrays for various variable types x=var_type
    # See Appendix A of Nataf & Schaeffer (2023)
    #
    # 231107 HCN : officialize multiplying LM and DNS spectra by n before conversion
    # 230608 HCN : *** test : divise tau(ell) by sqrt(n) ***
    # 230413 HCN : divide ell by 2 for DNS and LW
    # 230308 HCN : modified
    # 230305 HCN : *** to be completed ***
    #
    # inputs
    #   var_type : 'u', 'b' or 'rho'
    #   spec_type : 'K41', 'LM', 'DNS'
    #   for_ell :
    #       K41 : k [m^-1] array
    #       LM, DNS : n [] array
    #   for_tau :
    #       u and b :
    #           K41 : E(k) [m^3 s^-2] array 
    #           LM : LM(n) [kg m^2 s^-2] array (rho needed)
    #           DNS : S(n) [m^2 s^-2] array
    #       rho : (gravity needed)
    #           K41 : E(k) [] array
    #           LM : LM(n) [] array
    #           DNS : S(n) [] array
    #   param : dictionary of needed parameters (R_o, rho, gravity)
    #
    # outputs
    #   OK : True or False (error)
    #   ell : [m] 
    #   tau : [s]
    # --------------------------------------------------------------------
    """
    OK = True
# check dimensions of for_ell and for_tau
    if (len(for_ell) != len(for_tau)):
        OK = False
        print('*** tauell_from_spectrum : length of for_ell differs from length of for_tau')
        return (OK)
#
    R_o = param['R_o'] # [m] for ell
#
# Lowes-Mauersberger LM(n) spectrum
# - - - - - - - - - - - - - - - - -
# for_ell lists harmonic degree n
# for_tau lists energy density spectrum LM(n) [kg m^2 s^-2] ([] for rho)
#
    if (spec_type=='LM'):
        rho = param['rho'] # [kg m^-3] for LM spectra
        ell = 0.5*np.pi*R_o/(for_ell+0.5)
# u : kinetic energy spectrum
        if (var_type=='u'):
            tau = ell / np.sqrt(2*for_ell*for_tau/rho)
            return (OK, ell, tau)
# b : magnetic energy spectrum
        elif (var_type=='b'):
            tau = ell / np.sqrt(2*for_ell*for_tau/rho)
            return (OK, ell, tau)
# rho : (Delta_rho/rho)^2 spectrum
        elif (var_type=='rho'):
            gravity = param['gravity'] # [m s^-2] for tau_rho
            tau = np.sqrt(ell/gravity / np.sqrt(for_ell*for_tau))
            return (OK, ell, tau)
        else :
            OK = False
            print('*** tauell_from_spectrum : unrecognized variable type ' + var_type + ' for ' + spec_type)
            return (OK)
#
# DNS S(n) spectrum
# - - - - - - - - - - - - - - - - -
# for_ell lists harmonic degree n
# for_tau lists spectrum S(n) [m^2 s^-2] ([] for rho)
#
    elif (spec_type=='DNS'):
        ell = 0.5*np.pi*R_o/(for_ell+0.5)
# u : u^2 spectrum
        if (var_type=='u'):
            tau = ell / np.sqrt(for_ell*for_tau)
            return (OK, ell, tau)
# b : V_A^2 spectrum
        elif (var_type=='b'):
            tau = ell / np.sqrt(for_ell*for_tau)
            return (OK, ell, tau)
# rho : (Delta_rho/rho)^2 spectrum
        elif (var_type=='rho'):
            gravity = param['gravity'] # [m s^-2] for tau_rho
            tau = np.sqrt(ell/gravity / np.sqrt(for_ell*for_tau))
            return (OK, ell, tau)
        else :
            OK = False
            print('*** tauell_from_spectrum : unrecognized variable type ' + var_type + ' for ' + spec_type)
            return (OK)
#
# Spectral energy density k-spectrum E(k)
# - - - - - - - - - - - - - - - - - - - -
# for_ell lists wavenumber k [m^-1]
# for_tau lists spectral energy density E(k) [m^3 s^-2] ([] for rho)
#
    elif (spec_type=='K41'):
        ell = 1/for_ell
# u
        if (var_type=='u'):
            tau = ell**(3/2) / np.sqrt(for_tau)
            return (OK, ell, tau)
# b
        elif (var_type=='b'):
            tau = ell**(3/2) / np.sqrt(for_tau)
            return (OK, ell, tau)
# rho
        elif (var_type=='rho'):
            gravity = param['gravity'] # [m s^-2] for tau_rho
            tau = np.sqrt(ell/gravity / np.sqrt(for_tau))
            return (OK, ell, tau)
        else :
            OK = False
            print('*** tauell_from_spectrum : unrecognized variable type ' + var_type + ' for ' + spec_type)
            return (OK)
#
# - - - - - - - - - - - - - - - - -
    else :
        OK = False
        print('*** tauell_from_spectrum : unrecognized spectrum type ' + spec_type)
        return (OK)
       
# ------------------------------
def sec2year(t_sec):
# ------------------------------
# converts seconds into years for y secondary axis
    t_year = t_sec/(365*24*3600)
    return (t_year)
# ------------------------------
def year2sec(t_year):
# ------------------------------
# converts years into seconds for y secondary axis
    t_sec = t_year*(365*24*3600)
    return (t_sec)
# ------------------------------
def m2km(l_m):
# ------------------------------
# converts meters into kilometers for x secondary axis
    l_km = l_m/1000
    return (l_km)
# ------------------------------
def km2m(l_km):
# ------------------------------
# converts kilometers into meters for x secondary axis
    l_m = l_km*1000
    return (l_m)
    
# -------------------------------
def create_right_ticks(min, max):
# -------------------------------
# compute xticks of type 10^-2, 10^0, 10^2, etc
# NB : matplotlib then forgets ticks at 10^-1, 10^1, etc...
    pmin = np.ceil(np.log10(min))
    pmin = pmin + np.remainder(pmin,2) # to start ticks at nearest 10^(0±2n)
    pmax = np.log10(max*1.01) # get the last tick if right on
#    pticks = np.arange(pmin,pmax,1) # labels factor 10 apart
    pticks = np.arange(pmin,pmax,2) # labels factor 10^2 apart
    ticks = 10**pticks
    return (ticks)

# -----------------------------------------------------
def tau_ell_setup(title, xmin, xmax, ymin, ymax,y_sec):
# -----------------------------------------------------
# Create figure with common tau-ell plot properties
# y_sec = flag for secondary y-axis
#
# 230319 HCN : add colors dictionary
#
# create dictionary of colors : basis = Color Brewer 5-class Set 1
    colors = {}
    colors.update({'b':(228/255, 26/255, 28/255)})
    colors.update({'eta':(188/255, 26/255, 28/255)}) # modified from b
    colors.update({'Alfven':(148/255, 26/255, 28/255)}) # modified from b
    colors.update({'u':(55/255, 126/255, 184/255)})
    colors.update({'nu':(55/255, 126/255, 144/255)}) # modified from u
    colors.update({'rho':(255/255, 127/255, 0/255)})
    colors.update({'kappa':(77/255, 175/255, 74/255)})
    colors.update({'chi':(77/255, 135/255, 74/255)}) # modified from kappa
    colors.update({'day':(152/255, 78/255, 163/255)})
    colors.update({'Rossby':(152/255, 78/255, 123/255)}) # modified from day

    plt.rc('font', size=16) # font size of all labels, legend, etc
    cm = 1/2.54  # centimeters in inches
    xsize = 20*cm
    ysize = xsize * (np.log10(ymax/ymin)/np.log10(xmax/xmin)) # corrected 230309
    fig, ax = plt.subplots(num=title,figsize=(xsize,ysize))
#fig.name('scales for Kolmogorov universal turbulence')
# log x and y axis
    ax.set_xscale('log')
    ax.set_yscale('log')
# force same scale for both x and y axes
    ax.set_aspect('equal')
# x and y limits
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
# create x and y ticks of type 10^-2, 10^0, 10^2, etc
    xticks = create_right_ticks(xmin, xmax)
    yticks = create_right_ticks(ymin, ymax)
    ax.set_xticks(xticks)
    ax.set_yticks(yticks)
    ax.xaxis.set_ticks_position('both')
    if y_sec:
        ax.yaxis.set_ticks_position('left')
    else:
        ax.yaxis.set_ticks_position('both')
# title and labels
    ax.set(title=title + '\n')
    ax.set(xlabel='length scale [m]')
    ax.set(ylabel='time scale [s]')
# secondary axes
#    secax = ax.secondary_xaxis('top', functions=(m2km, km2m))
#    secax.set_xlabel('length scale [km]')
    if y_sec:
        secay = ax.secondary_yaxis('right', functions=(sec2year, year2sec))
        secay.set_ylabel('time scale [year]')
#
    fig.tight_layout()
    return (fig, ax, colors)
    
# --------------------------------------------
def function_intersect(l1, t1, n1, l2, t2, n2):
# --------------------------------------------
# small function to compute the (l,t) coordinates of
# the intersection of straight lines (in log space)
# for tau-ell regime diagrams
# inputs :
#	l1, t1 : coordinates of one point of curve 1
#	n1 : exponent of curve 1
#	l2, t2 : coordinates of one point of curve 2
#	n2 : exponent of curve 2
# outputs :
#	lint, tint : coordinates of the intersection
#
# one has : t1 = a1 l1^n1 and the same for 2
#
    lint = ((t2/l2**n2) * (l1**n1/t1))**(1./(n1-n2))
    tint = t1 * (lint/l1)**n1
    return(lint, tint)

# ----------------------------------------
def construct_power_markers_new(diff, M_o):
# ----------------------------------------
# construct dissipated power markers from diffusion (viscous, magnetic, ...)
# l_power, t_power : lists of markers at dissipations at power of 3
# index : dictionary giving the index of a requested exponent in these lists 
# label : dictionary giving the unit of a requested exponent
#
# SI prefixes dictionary
    prefix = {-24:'y',-21:'z',-18:'a',-15:'f',-12:'p',-9:'n',-6:'mu',-3:'m'}
    prefix.update({0:'',3:'k',6:'M',9:'G',12:'T',15:'P',18:'E',21:'Z',24:'Y'})
# create label and index dictionaries
    label, index = {}, {}
# create lists
    l_power, t_power = [], []
#
    ind = -1
    for ip in range (-8, 8):
        ind = ind+1
        exp = 3*ip
        index.update({exp:ind})
        label.update({exp:prefix[exp]+'W'}) # unit label ('W', 'kW', 'MW', etc
        P_diss = 10**exp # 1 marqueur tous les 3 ordres de grandeur en puissance dissipée
        t_power_ip = np.sqrt(M_o*diff/P_diss) # temps correspondant a cette dissipation
        l_power_ip = np.sqrt(t_power_ip*diff) # longueur sur ligne tau_nu correspondante
        l_power.append(l_power_ip)
        t_power.append(t_power_ip)

    return (l_power, t_power, index, label)

# ----------------------------------------------------------
def construct_QG_power_markers_new(l_Ekman, Omega, R_o, M_o):
# ----------------------------------------------------------
# construct power markers for dissipation in the Ekman layers
# l_power, t_power : lists of markers at dissipations at power of 3
# index : dictionary giving the index of a requested exponent in these lists 
# label : dictionary giving the unit of a requested exponent
#
# SI prefixes dictionary
    prefix = {-24:'y',-21:'z',-18:'a',-15:'f',-12:'p',-9:'n',-6:'mu',-3:'m'}
    prefix.update({0:'',3:'k',6:'M',9:'G',12:'T',15:'P',18:'E',21:'Z',24:'Y'})
# create label and index dictionaries
    label, index = {}, {}
# create lists
    l_power, t_power = [], []
#
    ind = -1
    for ip in range (-8, 8):
        ind = ind+1
        exp = 3*ip
        index.update({exp:ind})
        label.update({exp:prefix[exp]+'W'}) # unit label ('W', 'kW', 'MW', etc
        P_diss = 10**exp # 1 marqueur tous les 3 ordres de grandeur en puissance dissipée
        t_power_ip = (M_o*R_o*l_Ekman/Omega/P_diss)**(1./4.) # temps correspondant a cette dissipation
        l_power_ip = R_o/Omega/t_power_ip # longueur sur ligne tau_Rossby correspondante
        l_power.append(l_power_ip)
        t_power.append(t_power_ip)

    return (l_power, t_power, index, label)

# ------------------------------------------
def plot_wave(x_list, y_list, **wave_kwargs):
# ------------------------------------------
# plot a wavy (sine) line along tau(ell) line defined by ell = x_list and tau = y_list
# wave_kwargs contains optional argments (including line kwargs)
#
# number of points of the sine wave
    if 'npts' in wave_kwargs.keys():
        npts = wave_kwargs['npts']
    else:
        npts = 100
# number of cycles along the line
    if 'nper' in wave_kwargs.keys():
        nper = wave_kwargs['nper']
    else:
        nper = 20
# amplitude of the wiggles
    if 'famp' in wave_kwargs.keys():
        famp = wave_kwargs['famp']
    else:
        famp = 0.2
# kwargs arguments to be passed to plt.plot
    if 'line_kwargs' in wave_kwargs.keys():
        line_kwargs = wave_kwargs['line_kwargs']
    else:
        line_kwargs = {} 
#
# convert given (ell,tau) to log
    x_list = np.log(x_list)
    y_list = np.log(y_list)
# compute total length of segment
    total_length = 0
    for ip in range(len(x_list)-1):    
        segment = np.sqrt((x_list[ip+1]-x_list[ip])**2 + (y_list[ip+1]-y_list[ip])**2)
        total_length = total_length + segment
#
# determine period and amplitude from given parameters
    period = total_length/nper
    amp = period*famp
#
# initialize phase to 0 for first segment
    phi = 0
# loop on segments
    for ip in range(len(x_list)-1):
# build sine wave along x-axis on length of segment
        segment = np.sqrt((x_list[ip+1]-x_list[ip])**2 + (y_list[ip+1]-y_list[ip])**2)
        wave_x = np.array(range(npts))/npts*segment
        wave_y = amp * np.sin(2*np.pi*wave_x/period + phi)
# update phi for next segment
        phi = 2*np.pi*wave_x[-1]/period
# rotate sine wave along actual tau-ell trend
        alpha = np.arctan2(y_list[ip+1]-y_list[ip], x_list[ip+1]-x_list[ip])
        x_line = x_list[ip] + np.cos(alpha) * wave_x - np.sin(alpha) * wave_y
        y_line = y_list[ip] + np.sin(alpha) * wave_x + np.cos(alpha) * wave_y
# convert to exp and plot in loglog plot
        plt.plot(np.exp(x_line), np.exp(y_line), **line_kwargs)
 
# ------------------------------------
def construct_power_markers(diff, M_o):
# ------------------------------------
# construct dissipated power markers from diffusion (viscous, magnetic, ...)
    l_power, t_power = [], [] # create lists
    l_TW, t_TW = [], [] # create lists
    l_kW, t_kW = [], [] # create lists
    ip = 1
    while ip < 20:
        exposant = 3*(ip-7)
        P_diss = 10**exposant # 1 marqueur tous les 3 ordres de grandeur en puissance dissipée
        t_power_ip = np.sqrt(M_o*diff/P_diss) # temps correspondant a cette dissipation
        l_power_ip = np.sqrt(t_power_ip*diff) # longueur sur ligne tau_nu correspondante
        if (exposant==12): # cas TW pour marqueur spécifique
            l_TW = l_power_ip
            t_TW = t_power_ip
        if (exposant==3): # cas kW pour marqueur spécifique
            l_kW = l_power_ip
            t_kW = t_power_ip
        l_power.append(l_power_ip)
        t_power.append(t_power_ip)
        ip += 1
    return(l_power, t_power, l_TW, t_TW, l_kW, t_kW)
    
# ------------------------------------------------------
def construct_QG_power_markers(l_Ekman, Omega, R_o, M_o):
# ------------------------------------------------------
# construct power markers for dissipation in the Ekman layers
    l_power, t_power = [], [] # create lists
    l_TW, t_TW = [], [] # create lists
    l_kW, t_kW = [], [] # create lists
    ip = 2
    while ip < 16:
        exposant = 3*(ip-5)
        P_nu = 10**exposant # 1 marqueur tous les 3 ordres de grandeur en puissance dissipée
        t_power_ip = (M_o*R_o*l_Ekman/Omega/P_nu)**(1./4.); # temps correspondant a cette dissipation
        l_power_ip = R_o/Omega/t_power_ip # longueur sur ligne tau_Rossby correspondante
        if (exposant==12): # cas TW pour marqueur spécifique
            l_TW = l_power_ip
            t_TW = t_power_ip
        if (exposant==3): # cas kW pour marqueur spécifique
            l_kW = l_power_ip
            t_kW = t_power_ip
        l_power.append(l_power_ip)
        t_power.append(t_power_ip)
        ip += 1
    return(l_power, t_power, l_TW, t_TW, l_kW, t_kW)   
